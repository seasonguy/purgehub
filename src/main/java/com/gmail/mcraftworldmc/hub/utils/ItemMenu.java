package com.gmail.mcraftworldmc.hub.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.mcraftworldmc.hub.Main;

public class ItemMenu {
	private Main plugin;
	static ItemStack compass = setCustomItem(Material.COMPASS, (ChatColor.translateAlternateColorCodes('&', "&aGame Menu")), (ChatColor.AQUA + "Right-click to access the game menu!"));
	static ItemStack dye = null;
	static ItemStack pet = setCustomItem(Material.BONE, (ChatColor.GREEN + "Pets"), (ChatColor.AQUA + "Right click to summon a pet!"));
	private static Inventory menuInventory = Bukkit.createInventory(null, 9, ChatColor.translateAlternateColorCodes('&', "          &8&nQuick Game Menu"));
	public ItemMenu(Main instance){
		this.plugin = instance;
		menuInventory.setItem(2, setCustomItem(Material.DIAMOND_SWORD, (ChatColor.AQUA + "Factions"), (ChatColor.LIGHT_PURPLE + "Conquer the world!")));
		menuInventory.setItem(3, setCustomItem(Material.FIRE, (ChatColor.RED + "The Purge"), (ChatColor.DARK_RED + "Will you survive the annual Purge?")));
		menuInventory.setItem(4, setCustomItem(Material.GRASS, (ChatColor.GREEN + "Creative"), (ChatColor.AQUA + "Build what your heart desires!")));
		menuInventory.setItem(5, setCustomItem(Material.IRON_SWORD, (ChatColor.GOLD + "PvP"), (ChatColor.YELLOW + "Battle it out!")));
		ItemStack is = setCustomItem(Material.APPLE,(ChatColor.RED + "Ultra Hardcore"), (ChatColor.GRAY + "Coming soon!"));
		menuInventory.setItem(6, is);
	}
	public void addStartingItems(Player p){
		p.getInventory().clear();
		dye = setCustomItem(Material.INK_SACK, (ChatColor.GRAY + "Hide players"), (ChatColor.GREEN + "Hide players"));
		dye.setDurability((short) 10);
		p.getInventory().addItem(compass);
		p.getInventory().addItem(dye);
		plugin.book.addBook(p);
		p.getInventory().addItem(pet);
	}
	public ItemStack getCompass(){
		return compass;
	}
	public ItemStack getDye(){
		return dye;
	}
	public Inventory getMenuInventory(){
		return this.extracted();
	}
	private Inventory extracted() {
		return menuInventory;
	}
	private static ItemStack setCustomItem(Material m, String name, String lore){
		ItemStack i = new ItemStack(m);
		ItemMeta meta = i.getItemMeta();
		meta.setDisplayName(name);
		List<String> l =  new ArrayList<>();
		l.add(lore);
		meta.setLore(l);
		i.setItemMeta(meta);
		return i;
	}
}
