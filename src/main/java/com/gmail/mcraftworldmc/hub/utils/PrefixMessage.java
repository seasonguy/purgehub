package com.gmail.mcraftworldmc.hub.utils;

import org.bukkit.ChatColor;

public class PrefixMessage {
	public String prefix(){
		return (ChatColor.WHITE + "[" + ChatColor.AQUA + "Hub" + ChatColor.WHITE + "] ");
	}
	public String error(){
		return (ChatColor.RED + "ERROR" + ChatColor.GRAY + " >> ");
	}
}
