package com.gmail.mcraftworldmc.hub.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import com.gmail.mcraftworldmc.hub.Main;

public class Book {
	private Main plugin;
	public Book(Main instance){
		plugin = instance;
	}
	ItemStack book = new ItemStack(Material.WRITTEN_BOOK, 1);
	public void addBook(Player p){
		List<String> pages = new ArrayList<>();
		for(int i = 0; i < plugin.getConfig().getStringList("book.pages").size(); i++){
			pages.add(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getStringList("book.pages").get(i)));
		}
		BookMeta meta = (BookMeta) book.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("book.title")));
		meta.setPages(pages);
		meta.setAuthor(ChatColor.AQUA + "PurgeMC");
		List<String> lore = new ArrayList<>();
		lore.add(ChatColor.GOLD + "Information book for the PurgeMC!");
		meta.setLore(lore);
		book.setItemMeta(meta);
		p.getInventory().addItem(book);
	}
}
