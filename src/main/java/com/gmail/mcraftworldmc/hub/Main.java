package com.gmail.mcraftworldmc.hub;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.mcraftworldmc.hub.listeners.HubListener;
import com.gmail.mcraftworldmc.hub.utils.Book;
import com.gmail.mcraftworldmc.hub.utils.ItemMenu;
import com.gmail.mcraftworldmc.hub.utils.PrefixMessage;

public class Main extends JavaPlugin{
	public PrefixMessage mm = new PrefixMessage();
	public ItemMenu im = new ItemMenu(this);
	public Book book = new Book(this);
	public List<UUID> playersHiding = new ArrayList<>();
	public void onEnable(){
		if(Bukkit.getOnlinePlayers().length > 0){
			for(Player p : Bukkit.getOnlinePlayers()){
				p.getInventory().clear();
				im.addStartingItems(p);
				book.addBook(p);
			}
		}
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		saveDefaultConfig();
		getServer().getPluginManager().registerEvents(new HubListener(this), this);
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			sender.sendMessage(mm.error() + "Only players may use this command!");
			return true;
		}
		Player p = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("hubsetup")){
			getConfig().set("hubWorld", p.getWorld().getName() + "," + p.getLocation().getX() + "," + p.getLocation().getY() + "," + p.getLocation().getZ());
			p.sendMessage(mm.prefix() + "Successfully set hub spawn at your location!");
			saveConfig();
			return true;
		}
		if(cmd.getName().equalsIgnoreCase("hubsetpurge")){
			getConfig().set("servers.purge", p.getWorld().getName() + "," + p.getLocation().getX() + "," + p.getLocation().getY() + "," + p.getLocation().getZ());
			saveConfig();
			p.sendMessage(mm.prefix() + "Successfully set purge spawn at your location!");
			return true;
		}
		return false;
	}
}
