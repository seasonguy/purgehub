package com.gmail.mcraftworldmc.hub.listeners;

import io.github.dsh105.echopet.api.EchoPetAPI;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.mcraftworldmc.hub.Main;

public class HubListener implements Listener{
	private Main plugin;
	public HubListener(Main instance){
		this.plugin = instance;
	}
	EchoPetAPI ep = new EchoPetAPI();
	@EventHandler
	public void onJoin(final PlayerJoinEvent e){
		if(plugin.getConfig().getString("hubWorld") == null || plugin.getConfig().getString("hubWorld").isEmpty()){
			e.getPlayer().sendMessage(plugin.mm.prefix() + ChatColor.GREEN + "Hub world has not been set! It has been setted for you at your location! Please reference to the /hubsetup command!");
			plugin.getConfig().set("hubWorld", e.getPlayer().getWorld().getName() + "," + e.getPlayer().getLocation().getX() + "," + e.getPlayer().getLocation().getY() + "," + e.getPlayer().getLocation().getZ());
			plugin.saveConfig();
			return;
		}
		String[] split = plugin.getConfig().getString("hubWorld").split(",");
		e.getPlayer().teleport(new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3])));
		plugin.getLogger().info(e.getPlayer() + "Teleported to hub successfully!");
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){
			public void run(){
				plugin.im.addStartingItems(e.getPlayer());
			}
		}, 5l);
	}
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			if(e.getPlayer().getItemInHand() == null) return;
			ItemStack grayDye = new ItemStack(Material.INK_SACK, 1, (short) 8);
			ItemMeta m = grayDye.getItemMeta();
			m.setDisplayName(ChatColor.GREEN + "Show players");
			grayDye.setItemMeta(m);
			if(p.getItemInHand().equals(plugin.im.getCompass())){
				p.openInventory(plugin.im.getMenuInventory());
				return;
			}
			if(p.getItemInHand().equals(plugin.im.getDye())){
				plugin.playersHiding.add(p.getUniqueId());
				for(Player pl : Bukkit.getOnlinePlayers()){
					p.hidePlayer(pl);
				}
				p.setItemInHand(grayDye);
				p.sendMessage(ChatColor.YELLOW + "Hub" + ChatColor.GRAY + " >> " + ChatColor.GRAY + "You have hidden all players!");
				return;
			}
			if(p.getItemInHand().equals(grayDye)){
				plugin.playersHiding.remove(p.getName());
				for(Player pl : Bukkit.getOnlinePlayers()){
					p.showPlayer(pl);
				}
				p.sendMessage(ChatColor.YELLOW + "Hub" + ChatColor.GRAY + " >> " + ChatColor.GREEN + "You have revealed all players!");
				p.setItemInHand(plugin.im.getDye());
				return;
			}
			if(p.getItemInHand().getType().equals(Material.BONE)){
				ep.openPetSelector(p);
			}
		}
	}
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		switch(e.getItemDrop().getItemStack().getType()){
		default:
			e.setCancelled(false);
			break;
		case COMPASS:
		case INK_SACK:
		case WRITTEN_BOOK:
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onClick(InventoryClickEvent e){
		if(e.getInventory().getName().equalsIgnoreCase(plugin.im.getMenuInventory().getName())){
			e.setCancelled(true);
			Player p = (Player) e.getWhoClicked();
			if(e.getCurrentItem().getType().equals(Material.DIAMOND_SWORD)){
				p.closeInventory();
				if(plugin.getConfig().getString("servers.faction").equalsIgnoreCase("disabled")){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("settings.serverDisableMessage")));
					return;
				}
				this.connectToServer(p, "servers.faction");
				return;
			}
			if(e.getCurrentItem().getType().equals(Material.FIRE)){
				p.closeInventory();
				if(plugin.getConfig().getString("servers.purge").equalsIgnoreCase("disabled")){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("settings.serverDisableMessage")));
					return;
				}
				String[] split = plugin.getConfig().getString("servers.purge").split(",");
				p.teleport(new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3])));
				return;
			}
			if(e.getCurrentItem().getType().equals(Material.GRASS)){
				p.closeInventory();
				if(plugin.getConfig().getString("servers.creative").equalsIgnoreCase("disabled")){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("settings.serverDisableMessage")));
					return;
				}
				this.connectToServer(p, "servers.creative");
				return;
			}
			if(e.getCurrentItem().getType().equals(Material.IRON_SWORD)){
				p.closeInventory();
				if(plugin.getConfig().getString("servers.pvp").equalsIgnoreCase("disabled")){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("settings.serverDisableMessage")));
					return;
				}
				this.connectToServer(p, "servers.pvp");
				return;
			}
			if(e.getCurrentItem().getType().equals(Material.APPLE)){
				p.closeInventory();
				p.sendMessage(ChatColor.RED + "Coming soon! Can't wait? Donate now to help support this faster!");
				return;
			}
			p.closeInventory();
		}else{
			if(!e.getWhoClicked().isOp()) e.setCancelled(true);
		}
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		e.getDrops().clear();
	}
	@EventHandler
	public void onRespawn(final PlayerRespawnEvent e){
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){
			public void run(){
				plugin.im.addStartingItems(e.getPlayer());
				plugin.book.addBook(e.getPlayer());
			}
		}, 2l);
	}
	private void connectToServer(Player p, String configLine){
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream d = new DataOutputStream(bo);
		try{
			d.writeUTF("Connect");
			d.writeUTF(plugin.getConfig().getString(configLine));
		}catch(Exception e){
			plugin.getLogger().severe("Could not connect to server!");
			e.printStackTrace();
		}
		p.sendPluginMessage(plugin, "BungeeCord", bo.toByteArray());
	}
}
